.PHONY: all dev prod install install-requirements install-dev install-extras virtualenv init-submodules update-submodules reset-submodules clean

BASE_DIR?=${PWD}
ODOO_DIR?=${BASE_DIR}/odoo
CUSTOM_ADDONS_DIR?=${BASE_DIR}/custom_addons
VIRTUALENV_DIR?=${ODOO_DIR}/.env

all: init install

dev: init install install-dev

prod: all install-extras

install: virtualenv install-requirements

install-requirements: virtualenv
	${VIRTUALENV_DIR}/bin/pip install --upgrade pip
	# default odoo requirements
	${VIRTUALENV_DIR}/bin/pip install -r ${ODOO_DIR}/requirements.txt
	# default requirements for Merz b. Schwanen, e.g. Excel export
	${VIRTUALENV_DIR}/bin/pip install -r ${BASE_DIR}/requirements.txt

install-dev: virtualenv
	${VIRTUALENV_DIR}/bin/pip install --upgrade pip
	# requirements for development, e.g. watchdog for --dev=reload
	${VIRTUALENV_DIR}/bin/pip install -r ${BASE_DIR}/requirements-dev.txt

install-extras: virtualenv
	${VIRTUALENV_DIR}/bin/pip install --upgrade pip
	# TODO: extract actually PROD requirements for Merz b. Schwanen to facilitate deployment
	${VIRTUALENV_DIR}/bin/pip install -r ${BASE_DIR}/requirements-extras.txt

virtualenv:
	python3 -m venv ${VIRTUALENV_DIR}

init: init-submodules

init-submodules:
	# --progress is available from git 2.11.
	# Please update your git version or modify this line temporary. Note, that
	# the checkout takes long and you won't get feedback without --progress.
	git submodule update --init --recursive --progress

update-submodules: init-submodules
	# --progress is available from git 2.11, --remote for git 1.8.2 or above.
	# Please update your git version. Alternatively you may use:
	# git submodule foreach git pull origin master
	git pull --recurse-submodules
	git submodule update --recursive --remote --progress

reset-submodules:
	git submodule foreach --recursive git reset --hard

clean:
	# To remove files listed in the gitignores, use -xdf
	git clean -df
	git submodule foreach --recursive git clean -df
