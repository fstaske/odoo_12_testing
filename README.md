
# Collection of git repositories and scripts to start odoo for Merz b. Schwanen


## Developing

There is a Makefile provided containing the setup commands and a script
start.sh to start odoo correctly with the required addons. Please review
these files as part of the documentation.


### Checkout

In short, `git clone/pull` and `make` (or `make prod` on a server).

If you have you SSH key set up for bitbucket.org and github.com, you can
avoid username and password prompts that would otherwise be triggered for
every submodule. For this, clone the repository using git-Syntax

    git clone git@bitbucket.org:l9odoo/merz_b_schwanen.git

Note, using the "https://"-syntax will require you to add you password for
every checkout of a submodule.


#### Checkout/Update on a remote server

    cd <path_to_the_project>
    git pull  # or git fetch && git checkout your_branch
    make prod


##### Repository access

Eventually the server user accounts won't have access to our repositories.
Please use agent forwarding (`-A` option when logging in with SSH).

Keep in mind that you may need to add your SSH key to your session

    # ubuntu
    ssh-add -k
    # OS X
    ssh-add -K

(can be skipped when using a passphrase) and forward it with "-A":

    ssh -A <login>@<address>

You should login as your final user to forward the SSH agent
correctly. To switch the user after login, you can read  https://serverfault.com/questions/107187/ssh-agent-forwarding-and-sudo-to-another-user
and extend this README.md accordingly.


##### Troubleshoot

If `make` or `make init-submodules` does not work because of uncommitted
content in a submodule:

    # stash the changes
    cd <path_to_submodule>
    git stash

    # back to root and update
    cd ..
    make  # or make prod for server environment

    # pop your
    cd <path_to_submodule>
    git stash pop


### System Dependencies

Note, that you need git >= 2.11 (ubuntu >= 18.04).

The app starts in a virtualenv so you don't need to install pip packages with
`sudo` which might change your system configuration.

See https://virtualenv.pypa.io/en/stable/userguide

    sudo apt install python3-venv


On installation of new modules, less files will have to be preprocessed and
node-less is required:

    sudo apt install node-less


Further system dependencies for the pypi packages

    # to build native pip extensions
    sudo apt install python3-dev

    # lxml
    sudo apt install libxml2-dev libxslt-dev python-dev

    # pyldap
    sudo apt install libsasl2-dev libldap2-dev libssl-dev

Installation of pyldap may not work for python >= 3.6. In this case,
change the odoo/requirements entry to

    ldap3; sys_platform != 'win32'
    # pyldap==2.4.28; sys_platform != 'win32'


### Checkout submodules and install

Afterwards, checkout submodules and install dependencies in one
command:

    make

To install extra dependencies, e.g. for SSL and phonenumbers, use

    make install-extras


#### Commit your update of a submodule

After working on a submodule and committing your changes to your module's
repository, you also have to tell this repository about the new version:

    git add custom_modules/<your_module>
    git commit -m "Update <your_module>"

This will set the commit hash of your module inside this project to your
version. Co-workers will receive the update with `pull` and `make`.


#### Add a new submodule

    git submodule add git@bitbucket.org:l9odoo/<your_module>.git

Then modify the '.gitmodules' file and replace 'git@bitbucket.org:l9odoo'
with '..'. This way we avoid multiple password prompts. Afterwards add and
commit your changes:

    git add .gitmodules
    git commit -m "Add module <your_module>"


#### Detailed commands

Installation of the pip packages can be done with

    make install


If you just want to checkout the submodules to the current state of the
remote repository:

    make init-submodules


To upgrade all repositories to the latest commit (odoo and enterprise are
fixed to use branch 11).
Use this with caution! This task is mainly for testing or full-upgrade purposes
to see if there are new commits and to test them.

    make upgrade-submodules


### Start

You can use the provided start.sh. Note, that if you start the
odoo-bin manually, you have to activate the virtualenv before,
so that your installed requirements are available.

See https://virtualenv.pypa.io/en/stable/userguide/#activate-script

    odoo/.env/bin/activate
    python odoo-bin --addons-path=...

The important part is to use the python binary of the virtual env,
you can also do this with

    odoo/.env/bin/python3 ./odoo-bin --addons-path=...

or for system processed with absolute paths.


#### Production environment, optional requirements

Note, there are also optional dependencies, e.g. for phone number validations
or SSL:

    make prod

    # or just install new dependencies
    make install-extras

Odoo's PDF support does not require to install the python wkhtmltopdf
package, however, the apt package, unfortunately, creates a broken
PDF. You can use this aproach: https://l9projects.atlassian.net/wiki/spaces/LO/pages/252936193/wkhtmltopdf+Fehler+bei+Druck+aus+Odoo

See also:

* [How to restore a database dump](RESTORE_DB.md)
