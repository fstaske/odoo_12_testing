
# How to update this instance to a database miration provided by Odoo SA

## Restore the database dump and filestore

### Logged in as admin to the website

Navigate to <your_domain>/web/database/manager, chose "Restore Database"
and select your dump.


### In the command line

Given the database dump

    |
    \- filestore
      \- 12
      \- ...
    \- dump.sql

Create the database <your_database_name>:

    sudo -u postgres createdb <your_database_name>

For ubuntu local development, move the filestore

    mv filestore ~/.local/share/Odoo/filestore/<your_database_name>

Restore the databse dump to <your_database_name>

    sudo -u postgres psql <your_database_name> < dump.sql


## Update the included modules on the initial start

    ./start.sh -u all


Note, that there may be still various errors because of custom callbacks
for cron jobs, review to disable them in case. Also, file attachments are not
returned by Odoo SA and have to be copied accordingly.
