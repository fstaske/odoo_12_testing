#!/bin/bash

# Script to start the odoo bin. Usage

# ./start.sh <parameters>
#
# Examples:
#
# * ./start.sh --dev=reload  # to start in dev mode
# * BASE_DIR=/path/to/directory ./start.sh  # to start with custom BASE_DIR

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# set defaults
: "${BASE_DIR:=${SCRIPT_DIR}}"
: "${ODOO_DIR:=${BASE_DIR}/odoo}"
: "${VIRTUALENV_DIR:=${ODOO_DIR}/.env}"
: "${CONFIG:=${BASE_DIR}/odoo.local.conf}"

if [ ! -f $CONFIG ]; then
  echo "No $CONFIG found, falling back to default settings"
  CONFIG_PARAMETER=""
else
  CONFIG_PARAMETER="--config=${CONFIG}"
fi

# activate virtualenv
source ${VIRTUALENV_DIR}/bin/activate

# start odoo
cmd="${ODOO_DIR}/odoo-bin --addons-path=${BASE_DIR}/enterprise,${ODOO_DIR}/addons,${BASE_DIR}/custom_addons ${CONFIG_PARAMETER} ${@}"
echo $cmd
eval $cmd
